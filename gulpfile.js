var
    $ = require('gulp-load-plugins')({
        pattern: '*',
        lazy: false
    });
productionPath = './app/';

$.gulp.task('sass', function () {
    return $.gulp.src(['./_dev/_sass/style.scss'])
        .pipe($.compass({
            css: './_dev/_sass',
            sass: './_dev/_sass'
        })).on('error', log)
        .pipe($.gulp.dest('./_dev/_sass')).on('error', log);
});

$.gulp.task('jade', function () {
    return $.gulp.src('./_dev/_jade/_pages/*.jade')
        // вызов плагина gulp-jade
        .pipe($.jade({
            pretty: true
        })).on('error', log)
        .pipe($.gulp.dest('./_dev/_jade/_pages'));
});

$.gulp.task('css-minify', function () {
    return $.gulp.src('./app/css/**/*.css')
        .pipe($.minifyCss()).on('error', log)
        .pipe($.gulp.dest('./app/css'));
});


$.gulp.task('build', function () {
    var assets = $.useref.assets();

    $.rimraf.sync(productionPath, function (er) {
        if (er) throw er;
    });
    return $.gulp.src(['./_dev/_jade/_pages/*.html'])
        .pipe($.wiredep.stream({
            directory: './_dev/_bower'
        }))
        .pipe(assets).on('error', log)
        .pipe($.if('*.js', $.uglify())).on('error', log)
        .pipe(assets.restore()).on('error', log)
        .pipe($.useref()).on('error', log)
        .pipe($.gulp.dest(productionPath)).on('error', log);
});

// шрифты
$.gulp.task('fonts', function () {
    return $.gulp.src('./_dev/_sass/fonts/*')
        .pipe($.gulp.dest('./app/css/fonts'));
});
// изображения для стилей
$.gulp.task('css-img', function () {
    return $.gulp.src('./_dev/_sass/img/*')
        .pipe($.gulp.dest('./app/css/img'));
});
// изображения для стилей
$.gulp.task('img-fancy', function () {
    return $.gulp.src('./_dev/_sass/_fancybox/**/*')
        .pipe($.gulp.dest('./app/css'));
});

// изображения
$.gulp.task('img', function () {
    return $.gulp.src('./_dev/img/**/*')
        .pipe($.gulp.dest('./app/img'));
});

$.gulp.task('robots', function () {
    return $.gulp.src('./_dev/robots.txt')
        .pipe($.gulp.dest('./app/'));
});

$.gulp.task('build_css-minify', function (callback) {
    $.runSequence(['jade', 'sass'], 'build', ['css-img', 'img-fancy', 'robots', 'img', 'fonts'], 'css-minify', callback);
    //$.runSequence(['jade', 'sass'], 'build', ['css-img', 'img-fancy', 'robots', 'img', 'fonts'], callback);
});

$.gulp.task('watch', ['build_css-minify'], function () {
    $.gulp.watch(['./_dev/_jade/**/*.jade', './_dev/_js/**/*.js', './_dev/_sass/**/*.scss', './_dev/_sass/fonts/*'], ['build_css-minify']);
});

$.gulp.task('default', ['watch']);

function log(error) {
    console.log([
        '',
        "----------ERROR MESSAGE START----------",
        ("[" + error.name + " in " + error.plugin + "]"),
        error.message,
        "----------ERROR MESSAGE END----------",
        ''
    ].join('\n'));
    this.end();
}

