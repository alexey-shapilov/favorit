$(document).ready(function () {
    var
        slider = new Slider({
            buttons: {
                forward: 'slide_next',
                back: 'slide_prev'
            },
            slides: 'slide',
            container: 'slides'
        }),
        slider2 = new Slider({
            slides: 'describe-block',
            container: 'slides-flat',
            pager: {
                elem: 'pager_flat'
            }
        }),
        $tooltipster = $('.tooltip').tooltipster({
            animation: 'fade',
            position: 'right',
            trigger: 'custom',
            theme: 'my-custom-theme'
        });
});