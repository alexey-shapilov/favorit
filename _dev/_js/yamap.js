ymaps.ready(function () {
    var
        $city = $('.city'),
        city = ymaps.geolocation.city;

    $.each($city, function () {
        var
            $this = $(this);
        if (city == $this.text()) {
            var
                dropdown = $this.closest('.cities'),
                index = $this.attr('data-city-index');

            $city.removeClass('city_current');

            $city.each(function () {
                var $this = $(this);
                if ($this.attr('data-city-index') == index) {
                    $this.addClass('city_current');
                }
            });

            dropdown.each(function () {
                $(this).attr('data-selected', $this.attr('data-city-index'));
            });

            $('.city-current-text').text($this.text());
        }
    });
});

//1)   232241222@mail.ru  630132 г. Новосибирск Проспект Димитрова 7 офис 237. Т. 8(383) 381-42-25.
//
//2)Екатеринбург ул. МОСКОВСКАЯ д.82 т.83432213333 заказы на почту 2213333@bk.ru
//
//3) Питер  Россия, Санкт-Петербург,     заказы на mavlikaev@rbrgroup.ru sobolev@rbrgroup.ru sokolov@rbrgroup.ru nekrasov@rbrgroup.ru
//пр. Обуховской обороны, д. 120К
//(812) 495-49-50




//utm метки такие
//utm_campaign=spb-direct-office
//utm_campaign=msk-direct-office
//utm_campaign=ekb-direct-office
//utm_campaign=novosib-direct-office
//utm_campaign=saransk-direct-office
//utm_campaign=irkutsk-direct-office
//
//utm_campaign=spb-direct-kvartira
//utm_campaign=msk-direct-kvartira
//utm_campaign=ekb-direct-kvartira
//utm_campaign=novosib-direct-kvartira
//utm_campaign=saransk-direct-kvartira
//utm_campaign=irkutsk-direct-kvartira