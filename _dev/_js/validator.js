$(document).ready(function () {


    $('.btn_land-1').on('click', function () {
        var
            $form = $(this).closest('form'),
            $textValidation = $form.find('.textValidation'),
            $emailPhoneValidation = $form.find('.emailPhoneValidation');

        $.each($textValidation, function () {
            var
                $this = $(this);
            if ($this.val() == '' || $this.val() == $this.attr('placeholder')) {
                $this.tooltipster('show');
            }
            $this.on('keyup', function () {
                if ($this.val() !== '') {
                    $this.tooltipster('hide');
                } else {
                    $this.tooltipster('show');
                }
            })
        });

        $.each($emailPhoneValidation, function () {
            var
                $this = $(this);

            function checkEmailPhone(text) {
                if (!/^[\w\.\d-_]+@[\w\.\d-_]+\.\w{2,4}$/ig.test(text) || !/\d{11}/ig.test(text.replace(/[^\d]/ig, ''))) {
                    if (/^[\w\.\d-_]+@[\w\.\d-_]+\.\w{2,4}$/ig.test(text)) {
                        return true;
                    }
                    if (/\d{11}/ig.test(text.replace(/[^\d]/ig, ''))) {
                        return true;
                    }
                    return false;
                }
            }

            if ($this.val() == '' || $this.val() == $this.attr('placeholder')) {
                $this.tooltipster('show');
            }

            if (!checkEmailPhone($this.val())) {
                $this.tooltipster('show');
            }

            $this.on('keyup', function () {
                console.log('val', $this.val());
                if ($this.val() !== '') {
                    console.log(checkEmailPhone($this.val()));
                    if (checkEmailPhone($this.val())) {
                        $this.tooltipster('hide');
                    } else {
                        $this.tooltipster('show');
                    }
                } else {
                    $this.tooltipster('show');
                }
            })
        })
    })
});