function Slider(options) {
    var
        self = this,
        width = 0,
        height = 0,
        buttons = {};

    this.slider = {
        slides: $('.' + options.slides),
        container: $('.' + options.container),
        oldSlide: 0,
        currentSlide: 0
    };

    this.slider.countSlides = this.slider.slides.length;

    if (options.pager !== undefined) {
        this.pager = {
            el: $('.' + options.pager.elem),
            activeClass: options.pager.activeClass !== undefined ? options.pager.activeClass : 'pager_active'
        };
    }

    var slide = {};

    for (var i = 0; i < this.slider.countSlides; i++) {
        slide = $(this.slider.slides[i]);
        if (width < slide.outerWidth()) {
            width = slide.outerWidth()
        }
        if (height < slide.outerHeight()) {
            height = slide.outerHeight()
        }

        slide.css({
            position: 'absolute',
            width: '100%',
            display: 'none'
        });
    }

    if (this.slider.container.css('position') !== 'absolute' && this.slider.container.css('position') !== 'fixed') {
        this.slider.container.css({
            position: 'relative'
        });
    }

    this.slider.container.css({
        width: width,
        height: height
    });

    this.showSlide();

    if (options.buttons !== undefined) {
        buttons = {
            back: $('.' + options.buttons.back),
            forward: $('.' + options.buttons.forward)
        };

        buttons.back.on('click', function () {
            self.prevSlide();
        });

        buttons.forward.on('click', function () {
            self.nextSlide();
        });
    }

    if (this.pager !== undefined) {
        $.each(this.pager.el, function (index) {
            $(this).on('click', function () {
                self.showSlide(index);
            })
        });
    }

    this.getButtons = function () {
        return buttons
    };
}


Slider.prototype.showSlide = function () {
    var
        $oldSlide,
        $curSlide,
        $cssAnimation = $('html').hasClass('cssanimations');

    if (arguments[0] !== undefined) {
        this.setSlide(arguments[0])
    }

    $oldSlide = $(this.slider.slides[this.slider.oldSlide]);
    $curSlide = $(this.slider.slides[this.slider.currentSlide]);

    if (this.slider.currentSlide != this.slider.oldSlide) {
        if ($cssAnimation) {
            if ($oldSlide.hasClass('fadeIn')) {
                $oldSlide.toggleClass('animated fadeIn')
            }
            $oldSlide.toggleClass('animated fadeOut');
        } else {
            $oldSlide.css({
                display: 'none'
            });
        }
    }

    $curSlide.css({
        display: 'block'
    });

    if ($cssAnimation) {
        if ($curSlide.hasClass('fadeOut')) {
            $curSlide.toggleClass('animated fadeOut')
        }
        $curSlide.toggleClass('animated fadeIn')
    }

    if (this.pager !== undefined) {
        if (this.slider.oldSlide != this.slider.currentSlide) {

            if (this.pager.el[this.slider.oldSlide] !== undefined) {
                $(this.pager.el[this.slider.oldSlide]).toggleClass(this.pager.activeClass)
            }
            if (this.pager.el[this.slider.currentSlide] !== undefined) {
                $(this.pager.el[this.slider.currentSlide]).toggleClass(this.pager.activeClass)
            }
        } else if (this.slider.oldSlide == this.slider.currentSlide) {
            if (!$(this.pager.el[this.slider.currentSlide]).hasClass(this.pager.activeClass)) {
                $(this.pager.el[this.slider.currentSlide]).toggleClass(this.pager.activeClass)
            }
        }
    }

};

Slider.prototype.nextSlide = function () {
    if (this.slider.currentSlide < this.slider.countSlides - 1) {
        this.slider.oldSlide = this.slider.currentSlide;
        this.slider.currentSlide += 1;
        this.showSlide();
    }
};

Slider.prototype.prevSlide = function () {
    if (this.slider.currentSlide > 0) {
        this.slider.oldSlide = this.slider.currentSlide;
        this.slider.currentSlide -= 1;
        this.showSlide();
    }
};

Slider.prototype.setSlide = function (index) {
    if (index !== undefined) {
        if (index < 0) {
            index = Math.abs(index);

            if (index > this.slider.countSlides) {
                index = this.slider.countSlides - index % this.slider.countSlides
            } else {
                index = this.slider.countSlides % index
            }
        } else if (index + 1 > this.slider.countSlides) {
            index = index % this.slider.countSlides;
        }

        this.slider.oldSlide = this.slider.currentSlide;
        this.slider.currentSlide = index;
    }
};