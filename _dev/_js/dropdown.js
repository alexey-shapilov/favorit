(function ($) {
    $(document).ready(function () {
        $('.city-current').on('click', function () {
            var $this = $(this),
                dropdown = $this.closest('.cities');

            dropdown.toggleClass('cities_open');
        });

        $('.city').on('click', function () {
            var $this = $(this),
                dropdown = $this.closest('.cities'),
                index = $this.attr('data-city-index'),
                city = $('.city');

            city.removeClass('city_current');

            city.each(function () {
                var $this = $(this);
                if ($this.attr('data-city-index') == index) {
                    $this.addClass('city_current');
                }
            });

            dropdown.toggleClass('cities_open');

            $('.cities').each(function () {
                $(this).attr('data-selected', $this.attr('data-city-index'));
            });

            $('.city-current-text').text($this.text());
        });
    })
})(jQuery);