$(document).ready(function () {
    var
        $html = $('html');

    if ($html.hasClass('lt-ie9')) {
        $('input').placeholder();
    }
});
